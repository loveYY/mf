<?php
    namespace Database;

    /**
    * 数据库；连接类
    */
    class Connection
    {
        /*
        * 数据库配置信息
        */
        protected $config = [];

        function __construct()
        {
            $this->config = $this->getConfig();
        }

        /*
        * 数据库连接
        */
        public function connect()
        {
            $driver = ucfirst($this->config['driver']);
            $con = new 'Database\\'.$driver.'Connection';
            return $con ? true : false;
        }

        /*
        * 获取配置信息
        */
        protected function getConfig()
        {

        }
    }
?>