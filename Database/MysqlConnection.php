<?php
    namespace Database;

    /**
    * 数据库；连接类
    */
    class MysqlConnection
    {
        /*
        * 数据库配置信息
        */
        protected $config = [];

        function __construct($config = [])
        {
            $this->config = $config;
            $this->connect();
        }

        /*
        * 数据库连接
        */
        public function connect()
        {
            $con = mysql_connect(
                $this->config['DB_HOST'],
                $this->config['DB_DATABASE'],
                $this->config['DB_PASSWORD']
            );

            return $con ? true : false;
        }
    }
?>